package exemples;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Exemple de la classe Matcher i PAttern
 * @author manel
 */
public class Exemple2 {
    
    public static void main(String[] args) {
        
       //Cadena a analitzar
       String defText = "Ha estat molt poc Temps el que hi has dedicat....";
       
       // Expressió regular que fa match si:
       //    conté entre 3 i 6 caràcters de la "a" a la "z"
       //    ,seguida d'un caràcter que és una 't' o una 's'
       String expresioRegular = "[a-z]{3,6}[ts]";

       // Pattern verifica lexpressió regular
       // No es pot crear l'objecte si l'expressió és incorrecta a nivell sintàctic
       Pattern regles = Pattern.compile(expresioRegular);
       
       //Matcher busca l'expressió regular a la cadena i emmagatzema les N coincidencies
       Matcher matcher = regles.matcher(defText);
       
       //recorrem matcher per comptar les coincidencies
       System.out.println(String.format("S'han trobat %d coincidencies: ", matcher.results().count()));
       
       //tornem enrere el matcher (reset) per anar una per una des del principi
       matcher.reset();

        //Es repeteix tants cops com coincidencies (match) es trobin:
        while (matcher.find() == true){
            
            System.out.println();
            //mostrem la cadena coincident
            System.out.print(matcher.group() + ":   ");
            //resaltem el text coincident
            resaltaCaracters(defText, matcher.start(), matcher.end());
            System.out.println();

            // Més info de la classe Matcher a JAVADOC:
            // https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/regex/Matcher.html#groupCount()
        }
    }
    
    /***
     * Destaca en color els caràcters de txt situats entre ini i fi
     * @param txt
     * @param ini
     * @param fi 
     */
    private static void resaltaCaracters(String txt, Integer ini, Integer fi)
    {
        String ANSI_RESET = "\u001B[0m";
        String ANSI_GREEN = "\u001B[32m";
        
        for (int i=0; i<txt.length(); i++)
        {
            if (i==ini) System.out.print(ANSI_GREEN);
            if (i==fi) System.out.print(ANSI_RESET);
            System.out.print(txt.charAt(i));
            
        }
    }
    
}
