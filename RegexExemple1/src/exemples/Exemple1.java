package exemples;


/**
 * Exemple de mètode matches de la classe String
 * @author manel
 */
public class Exemple1 {
    
     public static void main(String[] args) {
        final String regex = "[0-9]{4}[BCDFGHJKLMNPRSTVWXYZ]{3}";
        //final String regex = "^[0-9]{4}[BCDFGHJKLMNPRSTVWXYZ]{3}$";
        final String cadena = "2344LJH";
        
        System.out.print("Verificat que l'expresió regular: ");
        System.out.println(regex);
        System.out.print( " i la cadena: ");
        System.out.println(cadena);
        
        if (!cadena.matches(regex))
            System.out.print(" NO ");
        
        System.out.println(" són totalment coincidents");    
    }
}
